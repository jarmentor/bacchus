import Head from '../components/Head';
import Navigation from '../components/Navigation';
import React from 'react';

export default class extends React.Component {
	static async getInitialProps({ req }) {
		const userAgent = req ? req.headers['user-agent'] : navigator.userAgent;
		return { userAgent };
	}

	render() {
		return (
			<div>
				<Head title="Jonathan Armentor" />
				<Navigation />
				<h1>Hi, I'm Jonathan</h1>
				<p>
					I'm is designer. All day, every day, I worries about the form, function, and placement of letters
					and the methods by which they exist. I have a strange compulsion to realign chairs at tables.
					Occasionally, I, indulge in a dry martini, <em>gin</em> not vodka, stirred, with a single twist of
					lemon. My personal interests as a designer extend to data systems, relationships, and their
					manipulation through intuitive user interfaces. Currently, I attend UL Lafayette and is pursuing a
					bachelor's degree in Visual Arts concentrated in Printmaking.
				</p>

				<small>{this.props.userAgent}</small>

				<style jsx>{``}</style>
			</div>
		);
	}
}
