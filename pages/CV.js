import Head from '../components/Head';
import Navigation from '../components/Navigation';
import React from 'react';

export default class extends React.Component {
	static async getInitialProps({ req }) {
		const userAgent = req ? req.headers['user-agent'] : navigator.userAgent;
		return { userAgent };
	}

	render() {
		return (
			<div>
				<Head title="JA: CV" />
				<Navigation />
				<h1>CV</h1>
				<p>
					Aliqua ex ad sunt labore esse veniam anim elit nulla adipisicing nostrud eu dolor est. Aute excepteur magna dolor qui excepteur sunt eu tempor. Culpa est consectetur minim do consequat fugiat adipisicing nulla sint deserunt officia. Fugiat magna minim ea nostrud sint aute duis veniam. Duis ex anim non id eu eu eu.
				</p>

				<small>{this.props.userAgent}</small>

				<style jsx>{``}</style>
			</div>
		);
	}
}
