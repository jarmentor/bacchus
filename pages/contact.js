import Head from '../components/Head';
import Navigation from '../components/Navigation';
import Footer from '../components/Footer';
import React from 'react';

export default class extends React.Component {

	render() {
		return (
			<div>
				<Head title="JA: Contact" />
				<Navigation />
				<h1>Contact</h1>
				<p>
					Aliqua ex ad sunt labore esse veniam anim elit nulla adipisicing nostrud eu dolor est. Aute
					excepteur magna dolor qui excepteur sunt eu tempor. Culpa est consectetur minim do consequat fugiat
					adipisicing nulla sint deserunt officia. Fugiat magna minim ea nostrud sint aute duis veniam. Duis
					ex anim non id eu eu eu.
				</p>

				<Footer />
			</div>
		);
	}
}
