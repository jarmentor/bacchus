import Link from 'next/link';
import React from 'react';

const NavItem = props => (
<div>		<style jsx>
			{`
				a {
					text-transform: uppercase;
				}
			`}
		</style>
	<Link href={props.href}>
		<a>{props.children}</a>
	</Link>
	</div>
);

export default props => (
	<div>

		<NavItem href="/">Home</NavItem>
		<NavItem href="/about">About</NavItem>
		<NavItem href="/blog">Blog</NavItem>
		<NavItem href="/work">Work</NavItem>
		<NavItem href="/CV">CV</NavItem>
		<NavItem href="/contact">contact</NavItem>
	</div>
);
