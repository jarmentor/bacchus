import Link from 'next/link';
import React from 'react';

export default class Footer extends React.Component {
	static async getInitialProps({ req }) {
		const year = new Date().getFullYear();
		const userAgent = req ? req.headers['user-agent'] : navigator.userAgent;
		return { year, userAgent };
	}

	render() {
		return (
			<div>
				<small>&copy; {this.props.year} Jonathan Armentor. All Rights Reserved.</small>
				<small>{this.props.userAgent}</small>
				<style jsx>{`
					small {
						display: block;
					}
				`}</style>
			</div>
		);
	}
}
