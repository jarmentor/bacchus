export const Colors = {
    primary: '#009b7b',
    secondary: '#1c1a1a',
	text: {
		headline: {
			light: '#009b7b',
			dark: '#1c1a1a'
		},
		body: '#6c6c6c'
	}
};


export const Typography = {
    primary: "'paralucent', sans-serif",
    secondary: "'Open Sans', Helvetica, Arial, sans-serif"
}