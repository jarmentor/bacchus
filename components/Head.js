import Head from 'next/head';
import { Colors, Typography } from './Styles';

export default props => (
	<div>
		<Head>
			<title>{props.title}</title>
			<meta name="viewport" content="initial-scale=1.0, width=device-width" />
			<link href="https://use.typekit.net/lzb3xjl.css" rel="stylesheet" type="text/css" />
			{props.children}
		</Head>

		<style jsx global>
			{`
				h1,
				h2,
				h3,
				h4,
				h5,
				h6 {
					font-family: ${Typography.primary};
					font-weight: 200;
					color: ${Colors.text.headline};
				}

				a,
				p,
				small {
					font-family: ${Typography.secondary};
					color: ${Colors.text.body};
				}

				a {
					text-decoration: none;
					color: inherit;
					margin: 15px;
				}

				a:hover {
					color: ${Colors.primary};
				}
			`}
		</style>
	</div>
);
